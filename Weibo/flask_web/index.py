from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from pymongo import MongoClient

# 连接mongodb
client = MongoClient('localhost', 27017)
# 连接数据库
db = client.weibo
# 连接表
collection = db.weibo


def create_app():
    app = Flask(__name__)
    Bootstrap(app)

    return app

app = create_app()

result = []

for item in collection.find():
    result.append(item)


@app.route('/')
def index():
    return repr(result)


@app.route('/user/<name>')
def user(name):
    return render_template('user.html', name=name)


if __name__ == '__main__':
    app.run(debug=True)
